## Arduino - using as a controller

Using an Arduino as controller.

* [How to control multiple DC motors with encoders](https://create.arduino.cc/projecthub/curiores/how-to-control-multiple-dc-motors-with-encoders-f8ed6c)

## motor driver boards

* [H Bridge Stepper Motor Driver (L9110S)](https://www.digitalimpuls.no/diverse/144586/h-bridge-stepper-motor-driver-l9110s-todelt-dc-motor-kontroll-modul)
* [L9110 Motor Driver with Arduino](https://www.electroniclinic.com/l9110-motor-driver-with-arduino-code-circuit-diagram/)
* [Interfacing L9110 / HG7881 Dual Channel Motor Driver Module with Arduino](https://electropeak.com/learn/interfacing-l9110s-dual-channel-h-bridge-motor-driver-module-with-arduino/)
* [Arduino & Visuino: Control DC Motor with L9110S Driver](https://create.arduino.cc/projecthub/mitov/arduino-visuino-control-dc-motor-with-l9110s-driver-ffae6d)

* [Dual H Bridge Motor Driver Modul (L298N)](https://www.digitalimpuls.no/diverse/144580/dual-h-bridge-motor-driver-modul-l298n-for-dc-and-stepper-motors-2-port)
* [nterface L298N DC Motor Driver Module with Arduino](https://lastminuteengineers.com/l298n-dc-stepper-driver-arduino-tutorial/)

* [Control DC Motors with L293D Motor Driver IC & Arduino](https://lastminuteengineers.com/l293d-dc-motor-arduino-tutorial/)
