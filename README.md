# Open lock controller

The idea is to create a lock controller (as in door lock) that is open hardware, and using open software to control it. It will use available hardware
components.

As a first step, look at some existing projects and products for ideas and inspiration.

## Documentation

Controller: use an [Arduino](docs/arduino_control.md), a Raspberry Pi,
