# Lock one

The first lock to be researched is from Phoniro, and is named Lock Base.
Label on the box and on the underside of the base has
```
Phoniro AB
380H2-D
SN:<serial number>
BT:< 12 hex digits>
```

The base is plastic the front / top cover is metal. There is a knob on the cover so you can turn the lock manually. The thick end of the cover has a
plastic piece that covers the battery compartment.

On the inside of the top / front cover is a label
```
K145-380DH2-LF
Rev. 7   Btach size: 400
SN: <serial number>
```

The innards of the Lock Base consists of a single pcb, it has connectors for battery and for motor, a geared DC motor and a large tooted wheel that
turns the lock.

## Motor

The motor is labeled (stamped into the metal housing)
```
ETONM MOTOR
ET-SGM25AGE-06198
DC6V 201703029
```
[Etonm](https://www.etonm.com/) has a product page for the [ET-SGM25A](https://www.etonm.com/Product_details/963727933943271424.html) series of round spur gear motors. If we compare the model number on this motor, it should be a DC 6 V motor (operation range 3.0 - 7.2 V) with a no load speed of 198 rpm. The encoder on the motor is currently unknown, but the [ET-MY24](https://www.etonm.com/Product_details/963740360992243712.html) magnet encoder is listed as suitable for the SGM25A. 